TARGET: bsk_lab2
CC	= gcc
CFLAGS	= -Wall -O2 -lpam -lpam_misc -g
LFLAGS	= -Wall
bsk_lab2: bsk_lab2.o
	$(CC) $(LFLAGS) $^ -o $@ -Wall -O2 -lpam -lpam_misc
.PHONY: clean TARGET
clean:
	rm -f bsk_lab2 *.o *~ *.bak
