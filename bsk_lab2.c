#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <stdio.h>
#include <stdbool.h>


static struct pam_conv pam_conversation_function = {
	misc_conv,
	NULL
};

void try_pam_operation(int return_code, char* error_message) {
	if (return_code != PAM_SUCCESS) {
		fprintf(stderr, error_message);
		exit(2);
	}
}

void login_to_application() {
	pam_handle_t* pam_handler = NULL;
	char *username = NULL;
	try_pam_operation(
		pam_start("bsk_lab2", username, &pam_conversation_function, &pam_handler),
		"Blad podczas inicjacji PAM");
	if (pam_handler == NULL) {
		try_pam_operation(PAM_ABORT, "pam_handler jest nullem\n");
	}
	try_pam_operation(
		pam_set_item(pam_handler, PAM_USER_PROMPT, "kto to?\n"),
		"Nie mozna zmienic domyslnej tresci zapytania o login\n"
		);
	try_pam_operation(
		pam_authenticate(pam_handler, 0),
		"Nieprawidlowy login lub haslo\n"
		);
	try_pam_operation(
		pam_acct_mgmt(pam_handler, 0),
		"Aplikacja nie moze byc uzywana w obecnej porze\n"
		);
	try_pam_operation(
		pam_end(pam_handler, PAM_SUCCESS),
		"Nie udalo sie zakonczyc sesji PAM"
		);
}

struct word {
	int letter;
	struct word* next;
};

struct word* new_word(int start_letter) {
	struct word* result = malloc(sizeof (struct word));
	result->letter = start_letter;
	result->next = NULL;
	return result;
}

struct word* append_letter_to_word(struct word* modified_word, int letter) {
	if (modified_word->next == NULL) {
		modified_word->next = new_word(letter);
	} else {
		append_letter_to_word(modified_word->next, letter);
	}
	return modified_word;
}

void free_word(struct word* word_to_free) {
	if (word_to_free == NULL) {
		return;
	}
	if (word_to_free->next != NULL) {
		free_word(word_to_free->next);
	}
	free(word_to_free);
}

bool equals(struct word* first, struct word* second) {
	if (first == NULL && second == NULL) {
		return true;
	}
	if (first == NULL || second == NULL) {
		return false;
	}
	if (first->letter != second->letter) {
		return false;
	}
	return equals(first->next, second->next);
}

void print_word(struct word* word_to_print) {
	if (word_to_print == NULL) {
		putchar('\n');
		return;
	}
	putchar(word_to_print->letter);
	if (word_to_print->next != NULL) {
		print_word(word_to_print->next);
	} else {
		putchar('\n');
	}
}

struct list_of_words {
	int number_of_occurences;
	struct word* actual_word;
	struct list_of_words* next;
};

const int NEW_WORD_CREATED = 1;
const int WORD_WAS_PRESENT_ON_LIST = 0;
const int NO_CHANGES = 2;

struct list_of_words* create_new_list(struct word* first_word) {
	struct list_of_words* result = malloc(sizeof (struct list_of_words));
	result->next = NULL;
	result->actual_word = first_word;
	result->number_of_occurences = 1;
	return result;
};

int add_word_to_list(struct list_of_words* list, struct word* word_to_add) {
	if (list == NULL || word_to_add == NULL || list->actual_word == NULL) {
		return NO_CHANGES;
	}
	if (equals(list->actual_word, word_to_add) == 1) {
		list->number_of_occurences++;
		return WORD_WAS_PRESENT_ON_LIST;
	}
	if (list->next != NULL) {
		return add_word_to_list(list->next, word_to_add);
	}
	list->next = create_new_list(word_to_add);
	return NEW_WORD_CREATED;
}

void free_list_of_words(struct list_of_words* list_to_free) {
	if (list_to_free == NULL) {
		return;
	}
	if (list_to_free->next != NULL) {
		free_list_of_words(list_to_free->next);
	}
	if (list_to_free->actual_word != NULL) {
		free_word(list_to_free->actual_word);
	}
	free(list_to_free);
}

void print_all_even_words(struct list_of_words* list_to_print) {
	if (list_to_print == NULL) {
		return;
	}
	if (list_to_print->number_of_occurences % 2 == 0) {
		printf("Slowo ");
		print_word(list_to_print->actual_word);
		printf(" wystepuje %d razy\n", list_to_print->number_of_occurences);
	}
	if (list_to_print->next != NULL ) {
		print_all_even_words(list_to_print->next);
	}
}

bool is_end_of_line(int readed_char) {
	if (readed_char == '.' || readed_char == '\n') {
		return true;
	}
	return false;
}


int is_end_of_word(int readed_char) {
	if (is_end_of_line(readed_char)) {
		return true;
	}
	if (readed_char == ' ') {
		return true;
	}
	return false;
}

void run() {
	int readed_char = 0;
	struct list_of_words* list = NULL;
	struct word* readed_word = NULL;
	do {
		readed_char = getchar();
		if (is_end_of_word(readed_char) == false) {
			if (readed_word == NULL) {
				readed_word = new_word(readed_char);
			} else {
				append_letter_to_word(readed_word, readed_char);
			}
		} else {
			if (readed_word != NULL) {
				if (list == NULL) {
					list = create_new_list(readed_word);
				} else {
					if (add_word_to_list(list, readed_word) 
						== WORD_WAS_PRESENT_ON_LIST) {
						free_word(readed_word);
					}
				}
				readed_word = NULL;
			}
			if (is_end_of_line(readed_char) && list != NULL) {
				print_all_even_words(list);
				free_list_of_words(list);
				list = NULL;
			}
		} 
	} while (readed_char != '.');
	
}


int main(int argc, char *argv[]) {
	login_to_application();
	printf("Logowanie zakonczone sukcesem\n");
	run();
	return 0;
}
